/*
Есть объект 
	asset{
		symbol,
		id,
	}, 
	project{
		id,
		assets,				prototype assetsArray
		assetId,
		total,
		sold,
		prices				prototype pricesArray
	}, 
	projectPrice{
		projectId,
		price,
		createdAt
	}

	Задача создать для каждого объекта класс.
	У кажого класса должны быть соответствующие поля.
	Создать прототип массива для каждого из этих классов 
		для asset метод поиска по id и по symbol (2 разных метода)
		для project метод поиска по id и assetId, добавить для массива метод который будет отдавать assetSymbol, lastPrice иииии sold и total
		для projectPrice поиск по projectId (Должно браться последнее добавленное (ориентироваться по createdAt))
*/

class Asset {
  constructor(symbol, id) {
    this.symbol = symbol;
    this.id = id;
  }
}

const assets = Array.from([
  new Asset("USPX", 5),
  new Asset("UKKN", 4),
  new Asset("DGTO", 3),
  new Asset("PRO", 2),
  new Asset("UDIG", 1),
]);

Array.prototype.getAssetById = function (id) {
  for (let index of this) {
    if (index.id === id) {
      return index;
    }
  }
};
Array.prototype.take

Array.prototype.getAssetBySymbol = function (symbol) {
  for (let index of this) {
    if (index.symbol === symbol) {
      return index;
    }
    console.log("Error incorrect entered data");
  }
};

class Project {
  constructor(id, assets, assetId, total, sold, prices) {
    this.id = id;
    this.assets = assets;
    this.assetId = assetId;
    this.total = total;
    this.sold = sold;
    this.prices = prices;
  }
}

Array.prototype.getProjetByAssetID = function (assetId) {
  for (let index of this) {
    if(index.assetId === assetId) {
      return index;
    }
    console.log("Error: Project with this Asset ID does not find")
  }
}

const projects = Array.from([
  new Project(1, assets[1], assets[1].id, 1000, 0, 100)
])

class ProjectPrice {
  constructor(projectId, price, createdAt) {
    this.projectId = projectId;
    this.price = price;
    this.createdAt = createdAt;
  }
}

function getDate() {
  let date = new Date();
  return date;
}

getDate();
