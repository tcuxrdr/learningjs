const arr = [
  {
    duty: "QWE",
    cost: 1,
  },
  {
    duty: "ASD",
    cost: 2,
  },
  {
    duty: "ZXC",
    cost: 3,
  },
];

function salariesSumm(){
  const initial = 0;
  const summOfManagers = this.managers.reduce((prev, curr) => prev + curr.salary, initial);
  const summOfEmployees = this.employees.reduce((prev, curr) => prev + curr.salary, initial);
  const total = summOfEmployees + summOfManagers;
  return {managersSalaries: summOfManagers, employeesSalaries: summOfEmployees, total: total};
}

const manamana = [
    {
      name: "Asfhan",
      id: "c07f199d-725f-417f-bf10-5ef7612d5cc5",
      employees: [],
      salary: 2000,
      isFired: false,
    },
    {
      name: "Caedyn",
      id: "7e4a6252-89e5-4d46-96cc-bdae8f25fc6c",
      employees: [],
      salary: 3000,
      isFired: false,
    },
    {
      name: "Byron",
      id: "152b998f-39cf-41fa-b34f-f8bda0564e01",
      employees: [],
      salary: 1579,
      isFired: false,
    },
    {
      name: "Majid",
      id: "8a0c5f8a-05ba-4148-8260-1b6f5a9cb759",
      employees: [],
      salary: 4555,
      isFired: false,
    },
    {
      name: "Mark",
      id: "f4fe6976-d5f8-4d39-b016-99989bd14809",
      employees: [],
      salary: 3943,
      isFired: false,
    },
  ]


  