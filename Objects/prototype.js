

function Car(name, year) {
	this.name = name;
	this.year = year;
};

Car.prototype.getAge = function() {
	return new Date().getFullYear() - this.year;
};

Car.prototype.toString = function () {
  return JSON.stringify(this)
};

let ford = new Car('Focus', 2012);
let bmw = new Car('X5', 2020)

console.log(ford);
console.log(bmw);

console.log(ford.toString());

// let arr = [3, 7, 4, 87, 14];

	Array.prototype.sum = function () {
		let result = 0;
		for (let value of this){
			result += value;
		}
		return result;
	}

	Array.prototype.sum1 = function() {
		let result = 0;
    for (i = 0; i < this.length; i++) {
    	const value = this[i];
    	result += value;
    }
    return result;
  }

//reduce / for each / map 
// console.log(arr.sum())
console.log(`Your sum = ${[3, 7, 4, 87, 14].sum()}`);

/*
Есть объект 
	asset{
		symbol,
		id,
	}, 
	project{
		id,
		assets,				prototype assetsArray
		assetId,
		total,
		sold,
		prices				prototype pricesArray
	}, 
	projectPrice{
		projectId,
		price,
		createdAt
	}

	Задача создать для каждого объекта класс.
	У кажого класса должны быть соответствующие поля.
	Создать прототип массива для каждого из этих классов 
		для asset метод поиска по iD и по symbol (2 разных метода)
		для project метод поиска по id и assetId, добавить для массива метод который будет отдавать assetSymbol, lastPrice иииии sold и total
		для projectPrice поиск по projectId (Должно браться последнее добавленное (ориентироваться по createdAt))
*/

