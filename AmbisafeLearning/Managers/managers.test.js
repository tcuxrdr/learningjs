import { Manager, managerCreator } from './managers';
import { randomName } from '../names';
import {generalObject} from '../functionsInsideObject'

const oldManagerLength = generalObject.managers.length;
const oldManagerArray = generalObject.managers;

describe("Tests of managers creation", () => {
	beforeAll(() => {
		managerCreator(randomName(), Math.floor(Math.random() * 5000));
	})
	afterAll(() => {
		generalObject.managers = oldManagerArray;
	})

	test("Check the managers creator method", () => {
		expect(generalObject.managers.length).toBe(oldManagerLength + 1);
	});

	test("Check that the manager created with empty employees array", () => {
		expect(generalObject.managers[generalObject.managers.length - 1].employees).toEqual([])
	})
});
