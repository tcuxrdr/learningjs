import { generalObject, guidGeneration } from "../functionsInsideObject.js";
import { namesArray, randomName } from '../names.js';

export class Manager {
  constructor(name, id, employees, salary, isFired = false) {
    this.name = name;
    this.id = id;
    this.employees = employees;
    this.salary = salary;
    this.isFired = isFired;
  }
}


// let firstManager = new Manager(
//   "Олег",
//   guidGeneration(),
//   generalObject.employees,
//   1000
// );
// console.log(firstManager);

export function managerCreator(managerName, salary) {
	let newManager = new Manager(
    managerName,
    guidGeneration(),
    [],
    salary
  );
	generalObject.managers.push(newManager);
}

