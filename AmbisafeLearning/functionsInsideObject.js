// var aguid = require('aguid')
import aguid from 'aguid'
import { dutiesList, addDuties, Duties } from './Duties/duties.js';

/*
	Создать объект у которого будет:
		1. Ключ "managers" в котором будет массив обектов менеджеров 
			структура манагеров: {
				name: 'string',
				id: guid,   wtf? npm aguid
				employees: [
					массив айдишников работников
				],
				salary: number,
				isFired: default false
			}
		2. Ключ "employees" в котором будет массив обектов работников 
			структура работника: {
				name: 'string',
				id: guid,   wtf? npm aguid
				duties: [
					массив стрингов (обязанностей)
				],
				salary: number,
				isFired: default false
			}
		3. Создать функции в объекте которые будут добавлять manager и employees -> DONE
		4. Создать функции в объекте которые будут удалять manager и employee, но удаление не будет происходить из массива -> DONE
			будет просто помечаться isFired: true 
		5. Создать функции изменения заработной платы для manager и employee -> DONE
		6. Создать функцию для привязки employee к какому то manager  -> DONE
		7. Создать функцию для добавления обязанностей для сотрудника -> DONE
		8. Создать функцию которая возвращает объект {                  // можно сделать с помощью reduce() -> DONE
			managersSalaries: сумма зарплат всех манагеров
			employySalaries: сумма зарплат всех работников
			totalSalaries: сумма всех зарплат
		}
		*9. Обращение к managers и employees должно происходить через this  -> DONE
		*10. После создания основы с массивами, переделать под localStorage
*/


export const generalObject = {
  managers: [
    {
      name: "Asfhan",
      id: "c07f199d-725f-417f-bf10-5ef7612d5cc5",
      employees: [],
      salary: 2000,
      isFired: false,
    },
    {
      name: "Caedyn",
      id: "7e4a6252-89e5-4d46-96cc-bdae8f25fc6c",
      employees: [],
      salary: 3000,
      isFired: false,
    },
    {
      name: "Byron",
      id: "152b998f-39cf-41fa-b34f-f8bda0564e01",
      employees: [],
      salary: 1579,
      isFired: false,
    },
    {
      name: "Majid",
      id: "8a0c5f8a-05ba-4148-8260-1b6f5a9cb759",
      employees: [],
      salary: 4555,
      isFired: false,
    },
    {
      name: "Mark",
      id: "f4fe6976-d5f8-4d39-b016-99989bd14809",
      employees: [],
      salary: 3943,
      isFired: false,
    },
  ],
  employees: [
    {
      name: "Amani",
      id: "64dfa123-e460-4382-9c32-ef1dd7a5edef",
      duties: [],
      salary: 187,
      isFired: false,
    },
    {
      name: "Alanas",
      id: "44130ead-c6ab-4f6c-b07c-e99b55df9d88",
      duties: [],
      salary: 848,
      isFired: false,
    },
    {
      name: "Tamiem",
      id: "e12f9680-61c7-4f73-9304-6929787286b6",
      duties: [],
      salary: 410,
      isFired: false,
    },
    {
      name: "Rana",
      id: "0985d1b5-4776-4d59-ba17-55cd7152bc57",
      duties: [],
      salary: 733,
      isFired: false,
    },
    {
      name: "Joris",
      id: "86977839-4ad8-4a31-a26f-2f0ab105fa49",
      duties: [],
      salary: 814,
      isFired: false,
    },
    {
      name: "Indy",
      id: "127ade06-d4cd-446c-a036-766f79f59f9e",
      duties: [],
      salary: 396,
      isFired: false,
    },
    {
      name: "Denon",
      id: "d20cb351-a433-4f52-bc79-169d3debb6d3",
      duties: [],
      salary: 231,
      isFired: false,
    },
    {
      name: "Shaun",
      id: "43c3d69d-de1d-4684-9167-278ad17f4ac6",
      duties: [],
      salary: 761,
      isFired: false,
    },
    {
      name: "Artur",
      id: "b1aa9bfb-242b-427b-8bf2-cca599142bb2",
      duties: [],
      salary: 944,
      isFired: false,
    },
  ],
  dutiesList,
  getUserById(array, id) {
    // Метод который вытаскивает юзера по ID
    let userId = array.find((item) => item.id === id);
    return userId;
  },
  salaryChanging(fromArray, id, newSalary) {
    // Метод для изменения юзеру ЗП
    let user = this.getUserById(fromArray, id);
    user.salary = newSalary;
    return user;
  },
  hireOrFireAnEmployee(fromArray, id, changeStatus = false) {
    // Метод для увольнения юзера
    let user = this.getUserById(fromArray, id);
    user.isFired = changeStatus;
    return user;
  },
  giveInToSubmission(managerId, ...args) {
    // Метод добавления в почдинение менеджеру сотрудников
    let manager = this.managers.find((item) => item.id === managerId);
    manager.employees.push(...args);
    return manager;
  },
  addDuties(dutyName, cost) {
    let newDuty = new Duties(dutyName, cost);
    const someResult = this.dutiesList.find((item) => item.duty === dutyName);
    if (!someResult) {
      return this.dutiesList.push(newDuty);
    }
    throw new Error("Error: Duty with this name already exist");
  },
  salariesSumm(){
    const initial = 0;
    const summOfManagers = this.managersSum()
    const summOfEmployees = this.employees.reduce((prev, curr) => prev + curr.salary, initial);
    const total = summOfEmployees + summOfManagers;
    return {managersSalaries: summOfManagers, employeesSalaries: summOfEmployees, total: total};
  },
  managersSum(){
    throw new Error('Error with salaries')
    const initial = 0;
    const summOfManagers = this.managers.reduce(
      (prev, curr) => prev + curr.salary,
      initial
    );
    return summOfManagers;
  },
};

// console.log(
//   main.getUserById(main.employees, "43c3d69d-de1d-4684-9167-278ad17f4ac6")
// );

// console.log(
//   // generalObject.salaryChanging(
//   //   generalObject.getUserById,
//   //   generalObject.employees,
//   //   "b1aa9bfb-242b-427b-8bf2-cca599142bb2",
//   //   7777
//   // )
// );

/* Список полезных методов
	массивов: 
		.map()
		.push()
		.filter()
		.find()
		.splice()
		.slice()
		.reverce()
		.reduce()
		.forEach()
			lifehack если нужно получить массив уникальных значений(что бы они не повторялись) 
			можно использовать new Set(array) возвращает массив с уникальными значениями.
		почитать:
			spread [...array]
			дуструктуризация 
			...rest
*/




export function guidGeneration() {
	const guid = aguid()
	return guid;
} 

// window.main = generalObject;
// window.addDuties = addDuties;
// export default generalObject;
