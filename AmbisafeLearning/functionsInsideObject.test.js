
import {generalObject} from './functionsInsideObject.js'
import {Duties, dutiesList } from './Duties/duties.js';

const oldSalary = generalObject.getUserById(
  generalObject.employees,
  "127ade06-d4cd-446c-a036-766f79f59f9e"
).salary;
const oldDutiesList = [
  new Duties("Clearing office", 300),
  new Duties("Customer Support", 600),
  new Duties("Coocking", 1500),
  new Duties("Office management", 400),
  new Duties("Clearing office", 300),
  new Duties("HRing", 700),
  new Duties("Call to clients", 1000),
];


describe("General object methods tests", () => {
  afterEach(() => {
    generalObject.salaryChanging(
      generalObject.employees,
      "127ade06-d4cd-446c-a036-766f79f59f9e",
      oldSalary
    );
  });

	test('Check the getUserById method return correct data', () => {
		expect(
      generalObject.getUserById(
        generalObject.managers,
        "f4fe6976-d5f8-4d39-b016-99989bd14809"
      )).toEqual({
        name: "Mark",
        id: "f4fe6976-d5f8-4d39-b016-99989bd14809",
        employees: [],
        salary: 3943,
        isFired: false,
      });
	});

  test('Check the salaryChanging method return correct data', () => {
    expect(
      generalObject.salaryChanging(
        generalObject.employees,
        "127ade06-d4cd-446c-a036-766f79f59f9e",
        500
      )
    ).toEqual({
      name: "Indy",
      id: "127ade06-d4cd-446c-a036-766f79f59f9e",
      duties: [],
      salary: 500,
      isFired: false,
    });
  });

  test('Check the salaryChanging method does not return old data', () => {
    expect(
      generalObject.salaryChanging(
        generalObject.employees,
        "127ade06-d4cd-446c-a036-766f79f59f9e",
        497
      )
    ).not.toEqual({
      name: "Indy",
      id: "127ade06-d4cd-446c-a036-766f79f59f9e",
      duties: [],
      salary: oldSalary,
      isFired: false,
    });
    generalObject.salaryChanging(
      generalObject.employees,
      "127ade06-d4cd-446c-a036-766f79f59f9e",
      oldSalary
    );
  });

  test('Check salaryChanging with toBe', () => {
    expect(
      generalObject.getUserById(
        generalObject.employees,
        "127ade06-d4cd-446c-a036-766f79f59f9e"
      ).salary
    ).toBe(oldSalary);
  });
});

describe('Check hireAndFireAnEmployee method', () => {
  afterEach(()=> {
    generalObject.hireOrFireAnEmployee(
      generalObject.managers,
      "152b998f-39cf-41fa-b34f-f8bda0564e01"
    );
  });

  test("Make sure that User to have property - isFired", () => {
    expect(
      generalObject.getUserById(
        generalObject.managers,
        "152b998f-39cf-41fa-b34f-f8bda0564e01"
      )
    ).toHaveProperty("isFired", false);
  });

  test("Make sure that hireAndFireAnEmployee mothod return correct data", () => {
    expect(
      generalObject.hireOrFireAnEmployee(
        generalObject.managers,
        "152b998f-39cf-41fa-b34f-f8bda0564e01",
        true
      )
    ).toEqual({
      name: "Byron",
      id: "152b998f-39cf-41fa-b34f-f8bda0564e01",
      employees: [],
      salary: 1579,
      isFired: true,
    });
  });
});

describe("Check giveInToSubmission method", () => {
  afterEach(()=> {
    generalObject.getUserById(
      generalObject.managers,
      "f4fe6976-d5f8-4d39-b016-99989bd14809"
    ).employees.length = 0;
  })

  test('Check that employee array is empty', () => {
    expect(
      generalObject.getUserById(
        generalObject.managers,
        "f4fe6976-d5f8-4d39-b016-99989bd14809"
      ).employees
    ).toEqual([]);
  });

  test("Check that giveInToSubmission work correctly and return expected result", () => {
    expect(
      generalObject.giveInToSubmission(
        "f4fe6976-d5f8-4d39-b016-99989bd14809",
        "d20cb351-a433-4f52-bc79-169d3debb6d3",
        "43c3d69d-de1d-4684-9167-278ad17f4ac6"
      )
    ).toEqual({
      name: "Mark",
      id: "f4fe6976-d5f8-4d39-b016-99989bd14809",
      employees: [
        "d20cb351-a433-4f52-bc79-169d3debb6d3",
        "43c3d69d-de1d-4684-9167-278ad17f4ac6",
      ],
      salary: 3943,
      isFired: false,
    });
  });

  test("Check that giveInToSubmission add employees to array", () => {
    const updatedManager = generalObject.giveInToSubmission(
      "f4fe6976-d5f8-4d39-b016-99989bd14809",
      "d20cb351-a433-4f52-bc79-169d3debb6d3",
      "43c3d69d-de1d-4684-9167-278ad17f4ac6"
    ).employees;

    expect(updatedManager).toContain("d20cb351-a433-4f52-bc79-169d3debb6d3");
    expect(updatedManager).toContain("43c3d69d-de1d-4684-9167-278ad17f4ac6");
  });
});

describe('Check addDuties method', () => {

  test('Check the Duties array', () => {
    expect(dutiesList).toEqual(oldDutiesList);
  });

  test('Check that addDuties method return correct data', () => {
    generalObject.addDuties("newDuty", 100500);
    expect(dutiesList[dutiesList.length - 1]).toEqual({ duty: 'newDuty', cost: 100500 })
  });
  
  test("Check that addDuties method return error", () => {
    expect(() => {
      generalObject.addDuties("newDuty", 100500);
    }).toThrow("Error: Duty with this name already exist");
  });
});

describe("Check salariesSumm method", () => {
  // test("Check that salariesSumm method return a number", () => {
  //   expect(generalObject.salariesSumm()).toEqual({
  //     managersSalaries: 15077,
  //     employeesSalaries: 5324,
  //     total: 20401,
  //   });
  // });

  // test("Check that method return object with values is Number", () => {
  //   expect(typeof(generalObject.salariesSumm().employeesSalaries)).toBe("number");
  //   expect(typeof generalObject.salariesSumm().managersSalaries).toBe("number");
  //   expect(typeof generalObject.salariesSumm().total).toBe("number");
  // })

  // test('Check the correct data of Object keys thet return', () => {
  //   expect(generalObject.salariesSumm().employeesSalaries).toBe(5324);
  //   expect(generalObject.salariesSumm().managersSalaries).toBe(15077);
  //   expect(generalObject.salariesSumm().total).toBe(20401);
  // });

  test('Check Salaries Sum with mocks', () => {
    const managersSpy = jest.spyOn(generalObject, 'managersSum')
    managersSpy.mockReturnValue(15077)
    expect(generalObject.salariesSumm().managersSalaries).toBe(15077);
  })

  test('Check salariesChanging', () => {
    const getUserId = jest.spyOn(generalObject, "getUserById");
    getUserId.mockImplementation((array, id) => {
      if(id === 'test') {
        return {name: 'test'}
      }
      return {}
    })
    expect(
      generalObject.salaryChanging(
        generalObject.employees,
        "64dfa123-e460-4382-9c32-ef1dd7a5edef",
        0
      )
    ).toEqual({salary: 0});
    expect(
      generalObject.salaryChanging(
        generalObject.employees,
        "test",
        0
      )
    ).toEqual({ salary: 0, name: 'test' });

  })
});
