

function isNumber (data) {
	if(isNaN(data)) {
		return new Promise((resolve, reject)=> {
			reject('error')
		})
	} else {
		if((data % 2) === 0){
			return new Promise((resolve)=> {
				setTimeout(() => resolve('even'), 2000)
			})
		}
			return new Promise((resolve) => {
        setTimeout(() => resolve("odd"), 1000);
      });
	}
}

function isNumber2(data){
	return new Promise((resolve, reject) => {
		if(isNaN(data)){
			reject('error')
		} 
			if((data % 2) === 0 ) {
				setTimeout(() => resolve('even'), 2000)
			} else {
				setTimeout(() => reject('odd'), 1000)
			}
	})
}



function isNumber3(){
	return 123;
}

async function asyncFunc(num){
 let value = await isNumber2(num);
 console.log(value)
 return value;
}