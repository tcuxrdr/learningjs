const action = text => {
	const action2 = (text) => text + "!";
	const resutl = transform2(action2(text));
	function transform2(str) {
    return str.replace(" ", "");
  }
	return resutl;
};

const action3 = text => transform(action2(text));

function transform(str) {
	return str.replace(" ", '');
}

const action2 = (text) => text + "!";


function actionWithCB(cb, ...args) {
	console.log('1')
	cb()
	console.log('2')
}

function CB1(cb) {
  setTimeout(() => {console.log("3"), cb()}, 1000);
};

async function CBPromiseAsync(){
	await new Promise((resolve) => {
		setTimeout(() => resolve(console.log("3")), 0);
	});
	console.log(3.1);
	return console.log(3.2);
}

function CBPromise(arr) {
  return new Promise((resolve) => {
    setTimeout(() => resolve(arr.push("1")), 0);
  })
    .then(() => arr.push("2"))
    .then(() => arr.push("3"))
    .then(() => arr.push("4"))
    .then(() => arr.push("5"))
    .then(() => arr.push("6"))
    .then(() => arr.push("7"))
    .then(() => arr.push("8"))
		.then(() => arr)
}

function actionWithCB2(cb, ...args) {
  console.log("1");
  cb( ()=> console.log('2') );
  // console.log("2");
}

function actionWithPromise(cb){
	return Promise.resolve()
    .then(() => ['apple', 'pineapple', 'lemon'])
    .then(cb)
    .then(arr => arr.map(val => val + 1))
    // .then(() => console.log("4"))
    // .then(() => console.log("5"))
    // .then(() => console.log("6"))
    // .then(() => console.log("7"))
    // .then(() => console.log("8"))
}