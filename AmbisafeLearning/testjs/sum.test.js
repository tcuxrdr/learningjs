const sum = require("./sum");

test("sum return object", () => {
  expect(sum()).toEqual({ a: 1, b: 2 });
});
