// ЗАдача 1 
const object = {
	sasha: 1,
	yehor: -1,
	larisa: 7,
	slava: 10
}

function getObjectKeys(obj) {
	return Object.keys(obj);
}

function getObjectValues(obj) {
	return Object.values(obj);
}

function getObjectKeysAndValues(obj){
	return Object.entries(obj);
  // for (let [key, value] of Object.entries(obj)) {
    // console.log(`${key}: ${value}`);
  // }

	// еще дописать сюда логику что бы пихать ключ - значение в новый объект

}

function getObjectFromEntries(obj){
	return Object.fromEntries(obj)
}

function swapKeysAndValues(obj) {
	const swap = Object.entries(obj);
	console.log(1)
	console.log(swap);
	
	const swap2 = swap.map(/*([key, value]) => [value, key]*/ gg);
	console.log(2);
  console.log(swap2);
	return Object.fromEntries(swap2);
}
//arr = [key , value]
function gg (arr) {
	return arr.reverse()
}

/*

1. Вытянуть значения с объекта использовать методы прототипа Object (вытянуть в массив)
2. Вытянуть ключи с объекта (вытянуть в массив)
3. Вывести с объекта key = 1, key = 2 (Ключ - значение) 

*/


// Задача 2
const nestedObject = {
  sasha: 1,
  yehor: {
		lesha: 3,
		slava2: {
			gomunkul: 5
		}
	},
  larisa: 7,
  slava: 10,
};

/*

1. Вытянуть значения с объекта использовать методы прототипа Object (вытянуть в массив)
2. Вытянуть ключи с объекта (вытянуть в массив)
3. Вывести с объекта key = 1, key = 2 (Ключ - значение) 
	P.S.  Ключи у которых значение = Object не учитывать, выводить только значниея внутри объектов
*/

// Задача 3

let str = 'Hello World!'

function stringToArray(string) {
	let resultArray = [];
	let replaceRegExp = /[^\w]/g;

	for (let i = 0; i < string.length; i++) {
      resultArray.push(string[i].replace(replaceRegExp, "*кряк*"));
  }
	return resultArray;
}


arrToObj = ['Q', 'A', 'S']

function makeArrayToObject(arr) {
	let obj = {}
	for (let i = 0; i < arr.length; i++) {
		obj[arr[i]] = `${arr[i]}`; 
	}
	return obj;
}
// попробовать с помощью reduce 

function stringToSymbol(string) {
	let resultArray = [];

	for (let i = 0; i < string.length; i++) {
		let regexp = /[a-zA-Z]/g;
		let replaceRegExp = /[-!$@# %^&*()_+|~=`{}\[\]:";'<>?,.\/]/g;
		let strSymbol = string[i];

		if (strSymbol !== regexp) {
			resultArray.push(string[i].replace(replaceRegExp, "*кряк*"))
			console.log(
        string[i].replace(replaceRegExp, "*кряк*")
      );
			
			// objectResult.string[i].replace(replaceRegExp, "*кряк*") = string[i].replace(replaceRegExp, "*кряк*")
		} else {
			console.log(string[i]);
			// objectResult.string[i] = string[i]
    }
	}
	return resultArray;
}

function countBy(x, n) {
  let z = [];
  for (let i = 1; i <= n; i++) {
		z.push(x * i);
	}
  return z;
}

let numArray = [1, 2, 3, 4, 5, 0, -1, -2, -3, -4]

function invertArray(array) {
	let result = [];
	for (let i = 0; i < array.length; i++) {
		result.push(-array[i])
	}
	return result;
}

function invertArray2(array) {
  let result = [];
  for (let item of array) {
    result.push(-item);
  }
  return result;
}

sheeps = [true, false, true, false, true, true, false]

function sheepFlock(array) {
	let sheepQuantity = [];
	for (let sheep of array) {
		if(sheep === true) {
			sheepQuantity.push(1)
		};
	};
	return sheepQuantity;
}

/*
	for (let item of array) где item - аналог array[i], а array это сам массив

	методы массивов (splice, slice, reduce, forEach, map)
*/
	


/*
	Написать функцию которая принимает строку и: 
		1. Выводит посимвольно (все что не является буквой заменить на фразу *кряк*)
		2. Создает объект где ключ и значение = символу 
		3. Создает объект в котором ключ это слово строки, а значение этого ключа {символ:символ}
		example "Hello world!" 
	{
		Hello : {
			H : "H",
			e: "e",
		},
		world!: {
			"w" : "w",
			"o": "o",
			"r": "r",
			"l": "l",
			"d": "d"
			"!": *кряк*
		}
	}		
																																
*/
