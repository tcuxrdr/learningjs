
let a = {
	aObj: 'aObj'
}

let b = {
	bObj : 'bObj',
	__proto__ : a
}

let c = {
	cObj : 'cObj',
	__proto__ : b
}

console.log(c)