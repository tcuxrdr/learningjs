import generalObject, { guidGeneration } from "../functionsInsideObject.js";
import { namesArray, randomName } from "../names.js";

class Employee {
  constructor(name, id, duties, salary, isFired = false) {
    this.name = name;
    this.id = id;
    this.duties = duties;
    this.salary = salary;
    this.isFired = isFired;
  }
}

function employeeCreator(employeeName, salary) {
  let newEmployee = new Employee(
    employeeName,
    guidGeneration(),
    generalObject.duties,
    salary
  );
  generalObject.employees.push(newEmployee);
}

employeeCreator(randomName(), Math.floor(Math.random() * 1000))
console.log(generalObject.employees);
