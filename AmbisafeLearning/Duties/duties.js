import generalObject from "../functionsInsideObject.js";

export class Duties {
  constructor(duty, cost) {
    this.duty = duty;
    this.cost = cost;
  }
}
export const dutiesList = [
  new Duties("Clearing office", 300),
  new Duties("Customer Support", 600),
  new Duties("Coocking", 1500),
  new Duties("Office management", 400),
  new Duties("Clearing office", 300),
  new Duties("HRing", 700),
  new Duties("Call to clients", 1000),
];



// console.log(dutiesList[1]);
